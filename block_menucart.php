<nav class="nav-right">
  <div class="nav_wrapper">
    <a class="close-mobile-menu noscroll" data-action="toggleMenuCart"><?php include("img/svg/close.svg.php"); ?></a>
    


     <?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

do_action( 'woocommerce_before_mini_cart' ); ?>

<?php if ( ! WC()->cart->is_empty() ) : ?>

  <ul class="woocommerce-mini-cart cart_list product_list_widget <?php echo esc_attr( $args['list_class'] ); ?>">
    <?php
      do_action( 'woocommerce_before_mini_cart_contents' );

      foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

          //  CUSTOM ITEM NAME 
          $taxonomy = 'pa_color';
          $meta = get_post_meta($_product->get_ID(), 'attribute_' . $taxonomy, true);
          $term = get_term_by('slug', $meta, $taxonomy);

          $product_title = $term->name . "<br/>" . $_product->get_title();

          $product_name      = apply_filters( 'woocommerce_cart_item_name', $product_title, $cart_item, $cart_item_key );
          $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
          $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
          $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
          ?>
          <li class="woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
            <?php
            echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
              '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
              esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
              __( 'Remove this item', 'woocommerce' ),
              esc_attr( $product_id ),
              esc_attr( $_product->get_sku() )
            ), $cart_item_key );
            ?>
            <?php if ( ! $_product->is_visible() ) : ?>
              <div class="woocommerce-item-img">
              <?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) . $product_name . '&nbsp;'; ?>
              </div>
            <?php else : ?>
              <a class="woocommerce-item-img" href="<?php echo esc_url( $product_permalink ); ?>">
                <?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail );?>
              </a>
              <a href="<?php echo esc_url( $product_permalink ); ?>" class="woocommerce-item-name"><?php echo $product_name; ?>
                
                <?php
                echo WC()->cart->get_item_data( $cart_item );
                 ?>
              </a>
              
            <?php endif; ?>
           

            <?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity-item">QTY: ' . sprintf( '%s', $cart_item['quantity']) . '</span>', $cart_item, $cart_item_key ); ?>

            <?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<div class="quantity">' . $product_price . '</div>', $cart_item, $cart_item_key ); ?>
          </li>
          <?php
        }
      }

      do_action( 'woocommerce_mini_cart_contents' );
    ?>
  </ul>

  <?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

  <p class="woocommerce-mini-cart__buttons buttons">
        <a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="checkout-button button alt wc-forward">
        <?php esc_html_e( "Proceed to checkout", 'woocommerce' ); ?>
        </a>
  </p>

  <p class="cart-button-container">
    <a href="<?php echo esc_url(wc_get_cart_url()); ?>" class="cart-button" ><?php _e("Go to cart!")?></a>
  </p>

<?php else : ?>

  <p class="woocommerce-mini-cart__empty-message"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></p>

<?php endif; ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>



    </div>
    
</nav>