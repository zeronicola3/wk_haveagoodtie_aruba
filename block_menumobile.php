<nav class="nav-left">

  <div class="nav_wrapper">
    <?php 
    /*  MENU MOBILE  */
    wp_nav_menu( array(
        'theme_location'  =>'menumobile' ,
        'container'       => '',
        'container_class' => "",
        'menu_class'      => 'main-menu',
      )
    );
      ?>

      <?php 
      /*  MENU MOBILE  */
      wp_nav_menu( array(
          'theme_location'  =>'menu_secondario' ,
          'container'       => '',
          'container_class' => "menu-secondario",
          'menu_class'      => 'secondary-menu',
        )
      );
        ?>
      <a class="close-mobile-menu noscroll" data-action="toggleMenu"><?php include("img/svg/close.svg.php"); ?></a>
    </div>
</nav>