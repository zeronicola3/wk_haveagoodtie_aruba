
<?php
	$postslist = get_posts( array(
	    'posts_per_page' => 3,
	    'order'          => 'DSC',
        'orderby' => 'date', 
	) );

	$post_nmb = 0;

	// Get the current date
	 $current_date = current_time( 'Y-m-d', $gmt = 0 );
	 $current_date = new DateTime($current_date);

	if ( $postslist ) { ?>
		<?php 
		$post_date = get_the_date( 'Y-m-d', $postslist[0]->ID );
		$post_date = new DateTime($post_date);
		$interval = $current_date->diff($post_date);
           if( $interval->days < 31 ) { 
         ?>

         <div class="news-container">
         	<h3><?php _e("diario", "webkolm"); ?></h3>

				 <?php foreach ( $postslist as $post ) :
				        setup_postdata( $post );

				        ?>
				        <a href="<?php the_permalink(); ?>" class="news-item">
				        
				        	<?php if( has_post_thumbnail()) {
			                      
			                      $id_immagine = get_post_thumbnail_id($post->ID);
			                      $thumb = wp_get_attachment_image_src( $id_immagine, 'medium' );
			                      $thumb_big = wp_get_attachment_image_src( $id_immagine, 'full' );

			                      ?>
			                      <div class="post-image-item img-c-<?php echo $post_nmb ?>">
			                        <style>
			                          .post-image-item.img-c-<?php echo $post_nmb ?>{ background-image:url('<?php echo $thumb['0'] ?>');}
			                            @media (min-width: 768px) {  .post-image-item.img-c-<?php echo $post_nmb ?> { background-image:url('<?php echo $thumb_big['0'] ?>'); } }
			                        </style>
			                      </div>
			                  <?php } ?>

				            <h5 class="news-title"> - <?php the_title(); ?></h5>
				        </a>

				    <?php
				    $post_nmb++;
				    endforeach; 
		}
	}
	    wp_reset_postdata();
?>
</div>