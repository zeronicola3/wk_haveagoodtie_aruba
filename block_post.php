

            <?php 
            $post_nmb=$post->ID;
            $gallery=types_render_field("post-gallery", array('post_id' => $id_pagina_intro, "raw"=>"true")); ?>

                <div class="post-meta">

                  <div class="post-meta-data">
                    <?php the_date('d.m.Y'); ?><br/>
                    <?php the_category(', '); ?>
                  </div>
                  <div class="post-meta-social">

                    <?php 
                      $social_url=get_the_permalink();
                      $social_title=get_the_title();
                      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
                      $social_media = $thumb['0'];
                    ?>

                    CONDIVIDI<br/>
                    <a href="http://www.facebook.com/share.php?u=<?php echo $social_url ?>&title=<?php echo $social_title?>" target="_blank" class="facebook-share">FB</a> - <a href="https://www.instagram.com/have_a_good_tie/" target="_blank" class="instagram-share">IG</a>

                  </div>
                </div>
                <div class="post-container">

                    <div class="post-title">
                      <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    </div>
                    <div class="post-thumb">
                      <?php if( has_post_thumbnail()) {
                          
                          $id_immagine = get_post_thumbnail_id($post->ID);
                          $thumb = wp_get_attachment_image_src( $id_immagine, 'medium' );

                          ?>
                          <div class="post-image-item img-c-<?php echo $post_nmb ?>">
                            <style>
                              .post-image-item.img-c-<?php echo $post_nmb ?>{ background-image:url('<?php echo $thumb['0'] ?>');}
                                
                            </style>
                          </div>
                      <?php } ?>
                    </div>


                      <div class="gallery-container">
                        <ul class="slides">
                        <?php

                        $post_content = $gallery;
                        if($post_content != "") {

                          preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
                          $array_id = explode(",", $ids[1]);

                          $numslide=1;
                          foreach ($array_id as &$item) {
                              $url_small = wp_get_attachment_image_src( $item, 'medium' );
                                
                                ?>
                                <li class="post_<?php echo $post_nmb; ?>_slide-<?= $numslide; ?> slideimg">
                                  <style>
                                    .post_<?php echo $post_nmb; ?>_slide-<?= $numslide; ?> { background-image:url('<?php echo $url_small['0'] ?>');}
                                  </style>
                                </li>
                                <?php $numslide++;
                          }

                        } else { 

                          $id_immagine = get_post_thumbnail_id($post->ID);
                          $thumb = wp_get_attachment_image_src( $id_immagine, 'medium' );
                          $thumb_big = wp_get_attachment_image_src( $id_immagine, 'full' );

                        ?>

                          <li class="post_<?php echo $post_nmb; ?>_slide-<?= $numslide; ?> slideimg">
                                <style>
                                  .post_<?php echo $post_nmb; ?>_slide-<?= $numslide; ?> { background-image:url('<?php echo $url_small['0'] ?>');}
                                    @media (min-width: 768px) {  .post_<?php echo $post_nmb; ?>_slide-<?= $numslide ?> { background-image:url('<?php echo $url_big['0'] ?>'); } }
                                </style>
                          </li>

                  <?php } ?>
                        
                        </ul>
                      </div>


                    <div class="post-content">
                        <?php the_content(); ?>

                        <?php $download=types_render_field("allegato-file", array('post_id' => $post->ID, "raw"=>"true")); 

                          if($download != "") {
                        ?>
                        <a class="download" href="<?php echo $download; ?>" download><?php _e("download", "webkolm"); ?></a>
                        <?php } ?>
                    </div>

                    <a class="read-more">
                      <span class="on"><?php _e('Read more', 'webkolm'); ?></span>
                      <span class="off"><?php _e('Close', 'webkolm'); ?></span>
                    </a>
                </div>

        