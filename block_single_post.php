<?php $gallery=types_render_field("post-gallery", array('post_id' => $id_pagina_intro, "raw"=>"true")); ?>

<div class="post-meta">

  <div class="post-meta-data">
    <?php the_date('d.m.Y'); ?><br/>
    <?php the_category(', '); ?>
  </div>
  <div class="post-meta-social">

    <?php 
      $social_url=get_the_permalink();
      $social_title=get_the_title();
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
      $social_media = $thumb['0'];
    ?>

    CONDIVIDI<br/>
    <a href="http://www.facebook.com/share.php?u=<?php echo $social_url ?>&title=<?php echo $social_title?>" class="facebook-share">FB</a> - <a href="#" class="instagram-share">IG</a>

  </div>
</div>

<div class="post-container single"> 

    <div class="post-title">
      <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </div>
      <div class="gallery-container">
        <ul class="slides">
        <?php

        $post_content = $gallery;
        if($post_content != "") {

          preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
          $array_id = explode(",", $ids[1]);

          $numslide=1;
          foreach ($array_id as &$item) {
              $url_small = wp_get_attachment_image_src( $item, 'medium' );
                
                ?>
                <li class="post_slide-<?= $numslide; ?> slideimg">
                      <style>
                        .post_slide-<?= $numslide; ?> { background-image:url('<?php echo $url_small['0'] ?>');}
                      </style>
                </li>
                <?php $numslide++;
          }

        } else { 

          $id_immagine = get_post_thumbnail_id($post->ID);
          $thumb = wp_get_attachment_image_src( $id_immagine, 'medium' );

        ?>

          <li class="post_slide-<?= $numslide; ?> slideimg">
                <style>
                  .post_slide-<?= $numslide; ?> { background-image:url('<?php echo $url_small['0'] ?>');}
                </style>
          </li>

  <?php } ?>
        
        </ul>
      </div>



    <div class="post-content">
        <?php the_content(); ?>
        <?php $download=types_render_field("allegato-file", array('post_id' => $post->ID, "raw"=>"true")); 

        
        ?>
        <a class="download" href="<?php echo $download; ?>" download><?php _e("download", "webkolm"); ?></a>
    </div>
   

</div>