<?php
get_header();

$page_diario = 7;
?>
<div id="contenuti">
    <div class="wrapper">
        <div class="wkrow">
      		<div class="wkcol-12 wp_content">
	          <div class="breadcrumb">
	              <span class="breadcrumb-start"><a href="<?php echo get_page_link(7); ?>" ><?php _e("diario","webkolm"); ?></a></span> > <?php the_category(); ?>
	          </div>
           <?php
        // Il Loop

        while ( have_posts() ) : the_post();?>
            <?php include("block_post.php"); ?>

            <?php endwhile;
               twentythirteen_paging_nav();
              // Reset Query
              wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>