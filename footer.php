        <!-- ALERT PAGINA PRODOTTO PERSONALIZZAZIONE -->

        <div class="wk-alert-personalizzazione">
            <?php _e('Fill one or both rows to proceed with the custom purchase.', 'webkolm');?>
        </div>

        <!-- BLUEIMPGALLERY -->
        <div id="singolo-overlay">
            <div id="loader-singolo"></div>
        </div>

        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev"></a>
        	<a class="next"></a>
        	<a class="close">x</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>

        <footer id="footer">
            <div class="wrapper">
            
               
                <?php
                $my_id = icl_object_id(53, 'page', false);
                $post_id = get_post($my_id);
                $content = $post_id->post_content;
                $content = apply_filters('the_content', $content);
                $content = str_replace(']]>', ']]>', $content);
                echo $content;
                
                ?>
            </div>
        </footer>
        <li class="cambiovaluta_wrapper onlymobile">
            <?php _e("currency", "webkolm" )?>: <?php do_action('wcml_currency_switcher', array('format' => '%symbol%')); ?>
        </li>

        <!-- CSS -->
        <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo bloginfo( 'stylesheet_directory' );?>/css/stile.css?v=3" />-->


        <!-- JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
        <!-- <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script> -->
        <script type="text/javascript" >
            /* PASSO VARIABILE LINGUA AL JS */
            var getLangCode = '<?php echo apply_filters( 'wpml_current_language', NULL );  ?>';
        </script>
        <script type="text/javascript" src="<?php echo bloginfo( 'stylesheet_directory' );?>/js/theme.js?v=9"></script>

        <?php 
            global $javascript_append;
            echo $javascript_append; ?>

        <?php get_template_part( 'cookie' ); ?>

        <?php wp_footer();?>
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-97688081-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-97688081-1');
        </script>

    </body>
</html>