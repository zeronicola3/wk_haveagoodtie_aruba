<?php /* if(!is_user_logged_in()) {
    header("Location: https://www.haveagoodtie.com/wp-content/themes/wk_haveagoodtie/static/index.php");
  } */
?>
<!DOCTYPE html>

<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> >
<!--<![endif]-->

<head>

  <!-- META -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-touch-fullscreen" content="YES">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes">

  <!-- FONT LIB -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

  <!-- JQUERY -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
  <!-- FAVICON -->
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/favicon.ico?v=1">


  <title><?php wp_title(''); ?></title>

    
  <!-- STYLE ABOVE THE FOLD -->
  <style><?php include 'css/atf.css'; ?></style>
	

  <?php

    global $woocommerce;
    //global $javascript_append;

    wp_head();

  ?>

  <!-- Hotjar Tracking Code for www.haveagoodtie.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:662985,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<meta name="google-site-verification" content="C1RQ1ezN6bJjjynnr1CbpiajMcIkbdHftBlK-bOKhMw" />
<style><?php include 'css/stile.css'; ?></style>
</head>

<body class="<?php if(is_front_page()){ echo 'home';} ?><?php if(is_home() || is_singular('post') || is_category()){ echo 'diario no-b-scroll';} ?>">
  <header>
    <div class="wrap">

      <!-- MENU MOBILE -->
      <?php include("block_menumobile.php"); 

        
         if (is_front_page()) {  

            $color_header = types_render_field("colore-voci-header", array('post_id' => $id_pagina_intro, "raw"=>"true"));

            if($color_header != "") {
              ?>
                  <style>
                      .color-header { color: <?php echo $color_header; ?> !important; }
                      .color-header svg { fill: <?php echo $color_header; ?>; }
                      .color-header svg * { fill: <?php echo $color_header; ?>; }
                      .color-header a { color: <?php echo $color_header; ?>; }
                      .color-header a::after { background-color: <?php echo $color_header; ?>!important; }
                      .color-header .wcml-dropdown.wcml_currency_switcher::after { background-color: <?php echo $color_header; ?>!important; }
                  </style>     

           <?php }
           
         } ?>

      <a class="mobile-menu-left text-menu-icon noscroll color-header" data-action="toggleMenu">MENU</a>
      <!-- MENU CART -->
      <a href="<?php echo get_home_url(); ?>" id="logo_sito" class="logo color-header">
        <?php include("img/svg/logo_hagt.svg.php"); ?>
      </a>

      <div class="right-menu-icons color-header" <?php if (is_front_page()) { echo 'style="color: ' . $color_header . ';"'; } ?>>
        <?php 



            
            ?>
            <script>
              function createCookie(name,value,days) {
                  var expires = "";
                  if (days) {
                      var date = new Date();
                      date.setTime(date.getTime() + (days*24*60*60*1000));
                      expires = "; expires=" + date.toUTCString();
                  }
                  document.cookie = name + "=" + value + expires + "; path=/";
              }
            </script>
            <?php

            // IMPOSTO IL COOKIE SULLE VALUTE
            $valuta_corrente=get_woocommerce_currency();
            //echo $valuta_corrente;

            //echo get_woocommerce_currency();


            if($_COOKIE['custom_currency']=="true"){
              // L'UTENTE È GIA STATO LOCALIZZATO

              // CONTROLLO SE IL COOKIE È AGGIORNATO
              if($_COOKIE['custom_currency_val']!=$valuta_corrente){
                  //echo "update cookie";
                  // ALTRIMENTI AGGIORNO COOKIE
                  ?>
                  <script>
                  createCookie('custom_currency_val','<?php echo $valuta_corrente;  ?>',365);
                  </script>
                  <?php

                  /* AGGIORNO LINGUA
                  function geo_client_currency($client_currency){
                      $client_currency = $valuta_corrente; //currency code
                      return $client_currency;
                  }
                  add_filter('wcml_client_currency','geo_client_currency');
                  */
              }

            }else{

              //echo "new cookie";

              // LOCALIZZIAMO L'UTENTE
              $country_utente=$woocommerce->customer->get_country( );

              global $cookie_value_cur;

              // IMPOSTIAMO LA VALUTA PER L'UTENTE
              if($country_utente=="US"){
                // IMPOSTA USA
                $cookie_value_cur = "USD";
                function geo_client_currency($client_currency){
                    $client_currency = 'USD'; //currency code
                    return $client_currency;
                }
                add_filter('wcml_client_currency','geo_client_currency');

              }elseif($country_utente=="JP"){
                // IMPOSTA YUAN
                $cookie_value_cur = "JPY";
                function geo_client_currency($client_currency){
                    $client_currency = 'JPY'; //currency code
                    return $client_currency;
                }
                add_filter('wcml_client_currency','geo_client_currency');
              }elseif($country_utente=="GB"){
                // IMPOSTA STERLINE
                $cookie_value_cur = "GBP";
                function geo_client_currency($client_currency){
                    $client_currency = 'GBP'; //currency code
                    return $client_currency;
                }
                add_filter('wcml_client_currency','geo_client_currency');
              }else{
                // IMPOSTA EURO
                $cookie_value_cur = "EUR";
                function geo_client_currency($client_currency){
                    $client_currency = 'EUR'; //currency code
                    return $client_currency;
                }
                add_filter('wcml_client_currency','geo_client_currency');
              }

              

              ?>
              <script>
                createCookie('custom_currency','true',365);
                createCookie('custom_currency_val','<?php echo $cookie_value_cur;  ?>',365);
              </script>
              <?php

              

              //echo get_woocommerce_currency();

            }

            // VISUALIZZO SWITCHER VALUTA

            ?><div class="nomobile"><?php
            do_action('wcml_currency_switcher', array('format' => '%symbol%'));
            ?></div><?php
          

        ?>
        <?php custom_language_selector(); ?>
        <a class="mobile-menu-right text-menu-icon" data-action="toggleMenuCart" <?php if (is_front_page()) { echo 'style="color: ' . $color_header . ';"'; } ?>>BAG(<?php echo WC()->cart->get_cart_contents_count(); ?>)</a>
      </div>

      <!-- MENU CART -->
      <?php include("block_menucart.php"); ?>

    </div>
  </header>