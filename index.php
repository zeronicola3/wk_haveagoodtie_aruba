<?php
/**
 * Template Name: Diario
 * 
 */
get_header();

?>
<div id="contenuti">
  <div class="wrapper">
    <div class="wkrow">
      <div class="wkcol-12 wp_content">
        <?php
        // Il Loop
        while ( have_posts() ) : the_post(); ?>

            <?php include("block_post.php"); ?>

            <?php endwhile;
               twentythirteen_paging_nav();
              // Reset Query
              wp_reset_query(); ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>