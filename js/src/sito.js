/* DIMENSIONI SCHERMO */
	 
	var $width = $(window).width();
	var $height = $(window).height();
	var $docheight = document.documentElement.scrollHeight;
	var $scrollspace = $docheight-$height;

function is_touch_device() {
 return (('ontouchstart' in window)
      || (navigator.maxTouchPoints > 0)
      || (navigator.msMaxTouchPoints > 0));
 //navigator.msMaxTouchPoints for microsoft IE backwards compatibility
}




	  
$(document).ready(function() {

	$(".webkolm-product-gallery.da_attivare").addClass("active").removeClass("da_attivare");

	$("input.addon-custom").attr("disabled", "disabled");
    	
	// OTTIMIZZAZIONE PER RETINA */
	
	if (window.devicePixelRatio == 2) {

          var images = $("img.hires");

          // loop through the images and make them hi-res
          for(var i = 0; i < images.length; i++) {

            // create new image name
            var imageType = images[i].src.substr(-4);
            var imageName = images[i].src.substr(0, images[i].src.length - 4);
            imageName += "@x2" + imageType;

            //rename image
            images[i].src = imageName;
          }
     }
	 
	
	if(!is_touch_device()){
		
		$(".touch").addClass("notouch");
		$("body").addClass("notouch");
		
	}
	
	if($width<768) {
		
		/* PER SMARTPHONE */
		
		//$cambiovaluta=$(".cambiovaluta-start").html();
		$(".cambiovaluta_wrapper").insertAfter(".cambiovaluta");

		
	} else {

	}


	// ALERT PERSONALIZZAZIONE CRAVATTA
	if($(".single-product")[0]){

		$wk_alert=$(".wk-alert-personalizzazione");
		$wk_alert.insertAfter( ".product-addon-campo-personalizzazione" );
	}


	/* TASTO PER APERTURA MENU */
	$("a.mobile-menu-left, .nav-left a.close-mobile-menu").on('click', function(){

	  	$('body').toggleClass('nav-open');
	  	
	  	if($(".home")[0]) {
	  		if(!$.scrollify.isDisabled()) {
	  		//	console.log("disabilito");
	  			$.scrollify.disable();
	  		} else {
	  	//		console.log("abilito");
	  			$.scrollify.enable();
	  		}
	  	}
	  
	  	var offset = $(window).scrollTop();

	  	if($width<1000) {
	  		$("body").toggleClass("bloccoscroll");
        	$("html").toggleClass("bloccoscroll");
			
			if($("body").hasClass('nav-open')){
				$('header').addClass('active');
			}else{
				$('header').removeClass('active');
			}
	  		
	  	}

	  	// Close menu if click outside

	  	if($("body").hasClass('nav-open')){
	  		$(document).mouseup(function(e) 
	  		{
	  		    var container = $(".nav-left");

	  		    // if the target of the click isn't the container nor a descendant of the container
	  		    if (!container.is(e.target) && container.has(e.target).length === 0) 
	  		    {
	  		        $('body').removeClass('nav-open');
	  		    }
	  		});
	  	}
	});


	$("a.mobile-menu-right, .nav-right a.close-mobile-menu").on('click', function(){

	  	$('body').toggleClass('bag-open');

	  	if($(".home")[0]) {
	  		if(!$.scrollify.isDisabled()) {
	  		//	console.log("disabilito");
	  			$.scrollify.disable();
	  		} else {
	  		//	console.log("abilito");
	  			$.scrollify.enable();
	  		}
	  	}

	  	var offset = $(window).scrollTop();

	  	if($width<1000) {

		  	$("body").toggleClass("bloccoscroll");
	        $("html").toggleClass("bloccoscroll");
	        

	        /*
	  		if(offset == 0 ) {
	  			$('header').toggleClass('active');
	  		}
			*/

			if($("body").hasClass('bag-open')){
				$('header').addClass('active');
				$(document).mouseup(function(e) 
				{
				    var container = $(".nav-right");

				    // if the target of the click isn't the container nor a descendant of the container
				    if (!container.is(e.target) && container.has(e.target).length === 0) 
				    {
				        $('body').removeClass('bag-open');
				    }
				});
			}else{
				$('header').removeClass('active');
			}

	  	}

	  	// Close bag if click outside
	  	if($("body").hasClass('bag-open')){
	  		$(document).mouseup(function(e) 
	  		{
	  		    var container = $(".nav-right");

	  		    // if the target of the click isn't the container nor a descendant of the container
	  		    if (!container.is(e.target) && container.has(e.target).length === 0) 
	  		    {
	  		        $('body').removeClass('bag-open');
	  		    }
	  		});
	  	}
	});
	
	$("nav.onlymobile .menu").on('click', 'li a', function(){
		var $this=$(this);
		if($this.hasClass("attivo"))
		{
			$this.toggleClass("attivo");
			$this.next('ul.sub-menu').slideToggle();
		}
		else{
			$this.next('ul.sub-menu').slideToggle();
			$this.toggleClass("attivo");	
		}
		
		if($(this).siblings("ul.sub-menu").size()!=0){
		  	return false;
		}
	});
	
	$("nav.onlymobile .menu li.current-menu-ancestor > a").addClass("attivo");


		


	/* SLIDER SITO */
/*
	if($(".webkolm-slider").length > 0){
		$('.webkolm-slider').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    start: function(slider) { slider.removeClass('loading');  },
		    slideshow: true,
		    slideshowSpeed : "4000",
		     pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true,
		    controlNav: true, 




		});
	}
*/
	if($(".gallery-container").length > 0){
			$('.gallery-container').flexslider({
			    animation: "fade",
			    animationLoop: true,
			    slideshow: false,
			    slideshowSpeed : "4000",
			     pauseOnHover: true,
			    multipleKeyboard: true,
			    keyboard: true,
			    controlNav: true, 

			});
		}


	if($(".webkolm-product-gallery").length > 0){
		if($width<768) {
            $(".webkolm-product-gallery").flexslider({
    		    animation: "fade",
    		    animationLoop: false,
    		    slideshow: false,
    		    slideshowSpeed : "4000",
    		    touch: false,
    		    pauseOnHover: true,
    		    multipleKeyboard: true,
    		    keyboard: true, 
    		    controlNav: "thumbnails",
    		    direction: "vertical",
    		});
	    } else {
	        $(".webkolm-product-gallery").flexslider({
    		    animation: "fade",
    		    animationLoop: false,
    		    slideshow: false,
    		    slideshowSpeed : "4000",
    		    touch: true,
    		    pauseOnHover: true,
    		    multipleKeyboard: true,
    		    keyboard: true, 
    		    controlNav: "thumbnails",
    		    direction: "vertical",
    		});
		}
		
	}



	$(".read-more").click(function (){
		var currentCont = $(this).prev(".post-content");
		$(currentCont).slideToggle(1000);
		$(this).toggleClass('read-open');

		if($(this).hasClass("read-open")) {
			$(this).siblings('.post-thumb').toggle();
			$(this).siblings('.gallery-container').toggle();
		} else {
			$(this).siblings('.post-thumb').toggle();
			$(this).siblings('.gallery-container').toggle();

			var pos_title = $(this);


			$('html, body').animate({
			    scrollTop: ($(pos_title).siblings(".post-title").first().offset().top + (-80))
			},500);
			console.log($(pos_title).siblings(".post-title"));
		}
		
		//var currentCont = $(this).closest(".post-container");
		//currentCont.toggleClass('read-open');
		//if(currentCont.hasClass('read-open')) {
		//	$(this).text("CLOSE");
		//} else {
		//	$(this).text("READ MORE");
		//}
	});


if($(".home")[0]) {
	$.scrollify({
		section : ".wk_b-scroll_row",
		sectionName : "blocco",
		interstitialSection : ".news-wrapper, #footer",
		easing: "easeOutExpo",
		scrollSpeed: 700,
		offset : -60,
		scrollbars: true,
		standardScrollElements: "body.diario, .woocommerce, .shop, .single-product",
		setHeights: false,
		overflowScroll: false,
		updateHash: true,
		touchScroll:true,
		before:function() {},
		after:function() {},
		afterResize:function() {},
		afterRender:function() {}
	});
}

	
$("textarea.addon-custom-textarea").attr("disabled", "disabled");

$('input.addon.addon-checkbox').change(function(){

	if($('input.addon.addon-checkbox').is(':checked')) {
		$('textarea.addon-custom-textarea').prop("disabled", false);
		$('.single_variation_wrap').addClass('activate');
		$(".single_add_to_cart_button").attr("disabled", "disabled");
		$(".webkolm-product-gallery").addClass("custom-image-active");
		$(".wk-alert-personalizzazione").addClass("activate");
		$(".webkolm-product-gallery.custom-image-active .flex-control-nav li:last-child img").trigger("click");
		
	} else {

		if($(".webkolm-product-gallery.custom-image-active .flex-control-nav li:last-child img").hasClass('flex-active')) {
			$(".webkolm-product-gallery .flex-control-nav li:first-child img").trigger("click");
		}

		$('.single_variation_wrap').removeClass('activate');
		$('.single_add_to_cart_button').prop("disabled", false);
		$("textarea.addon-custom-textarea").attr("disabled", "disabled");
		$(".webkolm-product-gallery").removeClass("custom-image-active");
		$(".wk-alert-personalizzazione").removeClass("activate");


	}
});

$('textarea.addon-custom-textarea').on('keyup', function() {
	if($(this).val().length > 0) {
		$('.single_add_to_cart_button').prop("disabled", false);
		$(".wk-alert-personalizzazione").removeClass("activate");
	} else {
		$(".single_add_to_cart_button").attr("disabled", "disabled");
		$(".wk-alert-personalizzazione").addClass("activate");
	}
});

$('.descrizione-singolo-prodotto').appendTo($('.single-product-desc'));


// GESTIONE NOMI E URL DEL SINGOLO PRODOTTO

// variabili nome prodotto / variazione / slug
var variation = $("span.swatch.selected").attr('data-value');
//$('.webkolm-product-gallery.gallery-' + variation).addClass("active");
var title = $("span.swatch.selected").attr('title');
var prod_title = $(".product_title.entry-title span.product-title").text();
var prod_slug = $(".product_title.entry-title").attr("data-product");

//$('.descrizione-' + variation ).addClass("activate");
//$(".product_title.entry-title span.color-attr").empty().html(" " + title);

//document.title = title + " " + prod_title;



$("span.swatch").on("click", function() {

	// variabili nome prodotto / variazione / slug
	var variation = $(this).attr('data-value');
	var title = $(this).attr('title');
	var prod_title = $(".product_title.entry-title span.product-title").text();
	var prod_slug = $(".product_title.entry-title").attr("data-product");

	$(".product_title.entry-title span.color-attr").empty().html(" " + title);
	$('.webkolm-product-gallery.active').removeClass('active');
	$('.webkolm-product-gallery.gallery-' + variation).addClass("active");

	$('.descrizione-singolo-prodotto.activate' ).removeClass("activate");
	$('.descrizione-' + variation ).addClass("activate");

	document.title = title + " " + prod_title;
	// OCIO QUI CHE SE SI SBAGLIA NON FUNZIONA CORRETTAMENTE L'ADD TO CART

	if(variation.endsWith("-en")) {
		window.history.pushState(variation, title + " " + prod_title, '/en/product/' + prod_slug + '/?attribute_pa_color=' + variation);
	} else {
		window.history.pushState(variation, title + " " + prod_title, '/prodotto/' + prod_slug + '/?attribute_pa_color=' + variation);
	}
});


/* if(window.location.href.indexOf("/?attribute_pa_color=") == -1) {

 	var url = window.location.href; 

 	if(window.location.href.indexOf("/product/") > -1) {

 		url = url.substring(0, url.length - 1);
 		console.log(url);
 		var arrVars = url.split("/");
 		var lastVar = arrVars.pop();
 		console.log(lastVar);

 		var color_attr = $(".tawcvs-swatches .swatch:first-child").attr('data-value');
 		//var color_attr = $(".tawcvs-swatches .swatch:first-child").attr('data-value');
 		window.location.replace('/en/product/' + lastVar + '/?attribute_pa_color=' + color_attr);
 		//window.history.pushState(color_attr,"", '/en/product/' + lastVar + '/?attribute_pa_color=' + color_attr);
 	} 
 }

	/* CAROUSEL POST

	$('.owl-carousel').owlCarousel({
	   loop:true,
	       margin:30,
	       responsiveClass:true,
	       autoplay:true,
	       dots:false,
	       nav:true,
	       navText:['&#60;','&#62;'],
	       responsive:{
	           0:{
	               items:1
	           },
	           600:{
	               items:2
	           },
	           1000:{
	               items:3
	           }
	           1280:{
	               items:4
	           }
	       }
	}); */

if($width<1000){
	
	$('#contenuti').waypoint(function(direction) {
	
		if($("body").hasClass('bag-open') || $("body").hasClass('nav-open')){
	
		}else{
			$("header").toggleClass('active', direction === 'down');
		}

	     
	   }, {
	     offset: '-1' // 
	});
	
}else{
	$('#contenuti').waypoint(function(direction) {
	     $("header").toggleClass('active', direction === 'down');
	   }, {
	     offset: '-1' // 
	});
}





// GESTIONE SCROLLIFY IN HOMEPAGE CON ABILITAZIONE E DISABILITAZIONE
// SOLO DA HOMEPAGE

if($("body").hasClass('home'))
{
console.log("home");

var previousScroll = 0; // VALORE DI SCROLL INIZIALE
var timer;				// TIMER PER LIMITARE FEEDBACK SCROLL
$ultimoelemento=$("body.home .wk_b-scroll_row").last(); 	// ULTIMA SLIDE DI SCROLLIFY
var top_of_element = $ultimoelemento.last().offset().top;	// TOP DELL'ULTIMA SLIDE SCROLLIFY
var top_diario=$(".diario_home_scroll").offset().top-58;	// TOP DEL DARIO (meno qualche pixel per evitare flash)
var bottom_of_element = $("body.home .wk_b-scroll_row").last().offset().top + $("body.home .wk_b-scroll_row").last().outerHeight(); // BOTTOM DELL'ULTIMA SLIDE SCROLLIFY
var top_of_element_offset=top_of_element+60;	// TOP DELLA ULTIMA SLIDE CON OFFSET PER HEADER

// FUNZIONE PER BLOCCARE PROPAGAZIONE
function stopScrolling (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
    
}


$(window).scroll(function(e) {
	// CREO UN TIMEOUT OGNI SCROLL
	clearTimeout(timer);
    timer = setTimeout(function() {

        // POSIZIONE SCROLL
        var currentScroll = $(this).scrollTop();
        
        // CONTROLLO PER SCROLL ATTIVATO A MANO
        var scrollup=0;
        
        // SE SCROLL UP
    	if(currentScroll < previousScroll){

    		// VALORE IN PIXEL DEL BORDO INFERIORE DELLA FINESTRA
    		var bottom_of_screen = $(window).scrollTop() + $(window).height();
    		// VALORE IN PIXEL DEL BORDO SUPERIORE DELLA FINESTRA
    		var top_of_screen = $(window).scrollTop();

    		//SE L'ULTIMA SLIDE SI VEDE E SONO POSIZIONATO SOTTO ALLA ULTIMA SLIDE SCROLLIFY
    		if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element) && (currentScroll > top_of_element)){
    			
    			// SE È GIA STATO FATTO UNO SCROLL MANUALE VERSO L'ALTO
    			if($("body.home .wk_b-scroll_row").last().hasClass("attivo") && scrollup == 0){

    				// NON ESEGUO NESSUN COMANDO E BLOCCO LA PROPAGAZIONE DELLO SCROLL
    				stopScrolling(e);
    			}else{

    				// ALTRIMENTI SPOSTO MANUALMENTE LA POSIZIONE DELLA PAGINA SULLA ULTIMA SLIDE SCROLLIFY
    				
    				// AGGIUNGO CLASSE PER CONTROLLO SUCCESSIVO
    				$("body.home .wk_b-scroll_row").last().addClass('attivo');

    				// ESEGUO ANIMAZIONE
    				$('html, body').animate({
    				       scrollTop: top_of_element_offset
    				   }, 500, function(){
    				   		
    				   		// ATTIVO VARIABILE -> È STATO FATTO UNO SCROLL MANUALE
    				   		scrollup = 1;

    				   		// SE DISABILITO ATTIVO SCROLLIFY
    				   		if($.scrollify.isDisabled()){
    				   			$.scrollify.enable();
    				   		}
    				   });
    			}
    		}
    		else{
    			//SE L'ULTIMA SLIDE NON È VISIBILE
    			// SE SIAMO NELLE SLIDE SOPRA L'ULTIMA
    			if(currentScroll < top_of_element){

    				// CONTROLLO SE SCROLLIFY SIA ABILITATO
    				if($.scrollify.isDisabled()){
    					$.scrollify.enable();
    				}
    			}	
    		}
        } // FINE SCROLL UP
        else if(currentScroll > previousScroll){
        	// INIZIO SE SCROLL DOWN

        	if(currentScroll > (top_of_element)){
        		// SE SIAMO SOTTO L'ULTIMA SLIDE

        		// RIPRISTINO VARIABILI E CLASSI ATTIVATE DALLO SCROLL MANUALE
        		scrollup=0;
        		$("body.home .wk_b-scroll_row").last().removeClass("attivo");

        		// ANIMO LO SCROLL AL DIARIO IN HOME	
        		if($.scrollify.isDisabled()){
        			// STOP PROPAGAZIONE SCROLL
        			stopScrolling(e);
        		}else{
        			// DISABILITO SCROLLIFY
        			$.scrollify.disable();

        			// ANIMO LA PAGINA POSIAZIONANDOLA SUL DIARIO
        			$('html, body').animate({
        				scrollTop: top_diario
        				}, 500, function(){
        			   		// STOP PROPAGAZIONE SCROLL
        			   		stopScrolling(e);
        			   	});
        		}
        	}
        }

        // AGGIORNO POSIZIONE SCROLL IN MEMORIA
        previousScroll = currentScroll;

    }, 100); // IMPOSTO UN VALORE DI TIMEOUT ADEGUATO PER LIMITARE FLASH E CALCOLI AL BROWSER
	 
});

} // FINE SCROLLIFY HOME

	// GESTIONE SPUNTA PER RICHIESTA FATTURA
	$('#myfield1_field').addClass('solo-se-fattura');
	$('#myfield2_field').addClass('solo-se-fattura');
	$('#myfield3_field').addClass('solo-se-fattura');
	$('#billing_fattura').on('click', function() {
	   if (!this.checked) {
	   		$('.solo-se-fattura').removeClass('superattivo');
	   }else{
	   		$('.solo-se-fattura').addClass('superattivo');
	   }
	});	


//	Dynamic shipping total updater

jQuery(document).on('change','#calc_shipping_country',function(){
	jQuery("[name='calc_shipping']").trigger("click");
});


$(".page-numbers::after").css("display: none;");

if(getLangCode=="it"){
	$("textarea.addon-custom-textarea").attr("placeholder", "riga 1\nriga 2");
}else{
	$("textarea.addon-custom-textarea").attr("placeholder", "row 1\nrow 2");
}


var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  
// Disable for chrome which already supports multiline
if (! (!!window.chrome && !isOpera)) {
$('textarea.addon-custom-textarea[placeholder]').each(function(index) {
  var text  = $(this).attr('placeholder');
  var match = /\r|\n/.exec(text);
  
  if (! match)
    return;
  
  $(this).attr('placeholder', '');
  $(this).attr('data-placeholder', text);
  $(this).addClass('active');
  $(this).val(text);
});

$('textarea.addon-custom-textarea[data-placeholder]').on('focus', function() {

  if ($(this).attr('data-placeholder') === $(this).val()) {
    $(this).attr('data-placeholder', $(this).val());
    $(this).val('');
    $(this).removeClass('active');
  }
});

$('textarea.addon-custom-textarea[data-placeholder]').on('blur', function() {
  if ($(this).val() === '') {
    var text = $(this).attr('data-placeholder');
    $(this).val(text);
    $(this).addClass('active');
  }
});
}



	var textArea = $('textarea.addon-custom-textarea');
    var maxRows = 2;
    var maxChars = 13;
    textArea.keypress(function(e){
        var text = textArea.val();
        var lines = text.split('\n');
        var check_nl = 0;

        if (e.keyCode == 13){
        	return lines.length < maxRows;
        }
        else{ //Should check for backspace/del/etc.
            var caret = textArea.get(0).selectionStart;
            
            if((caret==13) && (lines.length == 1)) {
            	textArea.val(textArea.val() + "\n");
            }
            
            var line = 0;
            var charCount = 0;
            $.each(lines, function(i,e){
                charCount += e.length;
                if (caret <= charCount){
                    line = i;
                    return false;
                }
                //\n count for 1 char;
                charCount += 1;
            });
                   
            var theLine = lines[line];
            return theLine.length < maxChars;
        }
    });
    




});/* FINE DOCUMENT READY */




