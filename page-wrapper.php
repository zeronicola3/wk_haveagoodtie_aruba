<?php
/**
 * Template Name: Woocommerce wrapper
 * 
 */
get_header(); ?>
<div id="contenuti" class="wc-wrap">
	<div <?php if(is_checkout()) { echo 'id="checkout"'; } ?> class="wrapper">


	        <?php
	            if ( have_posts() ) :
	                // Start the Loop.
	                while ( have_posts() ) : the_post();

	                    the_content();

	                endwhile;
	            endif;
	        ?>

	</div>
</div>
<?php get_footer(); ?>