<?php get_header(); ?>
<div id="contenuti">

        <?php
            if ( have_posts() ) :
                // Start the Loop.
                while ( have_posts() ) : the_post();

                    the_content();

                endwhile;
            endif;
        ?>

</div>
<?php get_footer(); ?>
