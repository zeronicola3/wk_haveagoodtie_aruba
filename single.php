<?php
get_header();

?>

    <div id="contenuti">
      <div class="wrapper">
        <div class="wkrow">
          <div class="wkcol-12 wp_content">

              <div class="breadcrumb">
                  <span class="breadcrumb-start"><a href="<?php echo get_page_link(7); ?>" ><?php _e("diario","webkolm"); ?></a></span> > <?php the_category(); ?> > <span class="breadcrumb-end"><a href="<?php the_permalink(); ?>" ><?php the_title() ?></a></span>
              </div>
              <?php
              // Il Loop
              while ( have_posts() ) : the_post(); ?>
               
                   <?php include("block_single_post.php"); ?>

              <?php $post_nmb++;  endwhile;
               twentythirteen_paging_nav();
              // Reset Query
              wp_reset_query(); ?>

            </div>
        </div>
    </div>
  </div>

<?php get_footer(); ?>