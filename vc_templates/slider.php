<?php


add_action( 'vc_before_init', 'wk_slider_build' );
function wk_slider_build() {

    vc_map( array(
        "name" => __( "Webkolm Image Slider", "webkolm" ),
        "base" => "webkolm_slider",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Create an image slider with text", 'webkolm'),
        "class" => "wk-slider",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Select dimension", "webkolm" ),
                "param_name" => "wk_slider_dimension",
                "value" => array( "full-page", "horizontal", "vertical", "squared" ),
                "description" => __( "Choose the ratio of the slider", "webkolm" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Caption style", "webkolm" ),
                "param_name" => "wk_slider_caption",
                "value" => array( "inside the slide", "out of slide" ),
                "description" => __( "Choose the position for the image caption", "webkolm" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Transition style", "webkolm" ),
                "param_name" => "wk_slider_transition",
                "value" => array( "fade", "slide" ),
                "description" => __( "Choose the ptype of the trasition", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Transition speed in ms", "webkolm" ),
                'param_name' => 'wk_slider_speed',
            ),
            //PARAMS GROUP
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'slides',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __( "Title", "webkolm" ),
                        'param_name' => 'wk_slide_title',
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __( "Subtitle", "webkolm" ),
                        'param_name' => 'wk_slide_subtitle',
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __( "Button", "webkolm" ),
                        'param_name' => 'wk_slide_button',
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __( "Link", "webkolm" ),
                        'param_name' => 'wk_slide_link',
                        "description" => __( "Insert url with http://, leave blank if you won't link the slide" , "webkolm" ),
                    ),
                    array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Select image", "webkolm" ),
                        "param_name" => "wk_slide_image",
                        "value" => "",
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __( "Image caption", "webkolm" ),
                        'param_name' => 'wk_slide_image_caption',
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => __( "Select text alignment", "webkolm" ),
                        "param_name" => "wk_slide_text_position",
                        "value" => array( "top-left", "top-center", "top-right", "center-left", "center-center", "center-right", "bottom-left", "bottom-center", "bottom-right" ),
                        "description" => __( "Choose the alignment of the text", "webkolm" )
                    ),
                    array(
                        "type" => "colorpicker",
                        "heading" => __( "Select color", "webkolm" ),
                        "param_name" => "wk_slide_text_color",
                        "value" => "",
                        "description" => __( "Color for text, defualt is white", "webkolm" )
                    ),
                )
            )
            
        )
    ) );
}


global $javascript_append;

add_shortcode( 'webkolm_slider', 'wk_slider_func' );
function wk_slider_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_slider_dimension' => 'full-page',
        'wk_slider_caption' => 'inside the slide',
        'wk_slider_transition' => 'fade',
        'wk_slider_speed' => '4000',
    ), $atts ) );
    $slides= vc_param_group_parse_atts( $atts['slides'] );

    // TIPOLOGIA DELLO SLIDER: wk_full-page, wk_horizontal, wk_vertical, wk_squared
    $slider_class=" wk_".$wk_slider_dimension." ";

    // TIPOLOGIA DI CAPTION DELLO SLIDER
    if($wk_slider_caption=="inside the slide"){ $slider_class.=" wk_caption_inside "; }
    if($wk_slider_caption=="out of slide"){ $slider_class.=" wk_caption_outside "; }
    
    // VELOCITA
    $velocita_slider=$wk_slider_speed;

    // TIPOLOGIA DI TRANSIZIONE
    $transizione_slider=$wk_slider_transition;

    // RANDOM ID SLIDER
    $id_slider=rand(0,99999);


    // CREO SLIDER FLEXSLIDER
    $output.='<div id="slider-wk-'.$id_slider.'" class="webkolm-slider '.$slider_class.'" ><ul class="slides">';
     
    $numslide=0;
    // CICLO LE SLIDES
    foreach( $slides as $slide ){
        $images_small = wp_get_attachment_image_src($slide['wk_slide_image'], 'medium')[0];
        $images_big = wp_get_attachment_image_src($slide['wk_slide_image'], 'large')[0];
        $images = wp_get_attachment_image_src($slide['wk_slide_image'], 'full')[0];

        // CHECK LINK

        $slidelink="";
        $closelink="";
        $pulsante="";
        $link=0;

        // CONTROLLO SE È DEFINITO UN PULSANTE
        if($slide['wk_slide_button']!=""){
            if($slide['wk_slide_link']!=""){
                $pulsante='<a class="pulsante" href="'.$slide['wk_slide_link'].'">'.$slide['wk_slide_button'].'</a>';
                $link=1;
            }else{
                $pulsante='<a class="pulsante">'.$slide['wk_slide_button'].'</a>';
            }
        }

        if($slide['wk_slide_link']!="" && $link=0){
            $slidelink='<a href="'.$slide['wk_slide_link'].'" class="slidelink">';
            $closelink="</a>";
        }

        // CHECK CAPTION
        $caption="";
        if($slide['wk_slide_image_caption']!=""){
            $caption='<div class="caption"><span>'.$slide['wk_slide_image_caption'].'</span></div>';
        }


        // CLASS LI OUTSIDE CAPTION
        $li_class="";
        if($caption!="" && $wk_slider_caption=="out of slide"){
            $li_class=" out_caption ";
        }

        // CHECK TITLE AND SUBTITLE

        $testo_slide="";
        if($wk_slider_dimension=="full-page"){
            $testo_slide='<div class="testo_slide '.$slide['wk_slide_text_position'].' " style="color:'.$slide['wk_slide_text_color'].'">
                    <h1 style="color:'.$slide['wk_slide_text_color'].'">'.$slide['wk_slide_title'].'</h1>
                    <h6 class="sottotitolo">'.$slide['wk_slide_subtitle'].'</h6>
                    '.$pulsante.'
                </div>';
        }else{
            $slidelink="";
            $closelink="";
        }   


        $output.='
            <style>
              .slideimg-'.$id_slider.'-'.$numslide.' { background-image:url('.$images_small.');}
              @media (min-width: 768px) {  .slideimg-'.$id_slider.'-'.$numslide.' { background-image:url('.$images_big.'); } }
              @media (min-width: 1800px) {  .slideimg-'.$id_slider.'-'.$numslide.' { background-image:url('.$images.'); } }
            </style>
            <li class="slide-'.$id_slider.'-'.$numslide.' '.$li_class.' ">
                <div class="slideimg-'.$id_slider.'-'.$numslide.' slideimg">
                    '.$caption.'
                </div>
                '.$slidelink.'
                    '.$testo_slide.'
                '.$closelink.'
            </li>';

        $numslide++;
    }

    // CHIUDO SLIDER
    $output .='</ul></div>';
    



    // JS SLIDER INIZIALIZZAZIONE
    global $javascript_append;
    $javascript_append.='
        <script>
            $("#slider-wk-'.$id_slider.'").flexslider({
                animation: "'.$transizione_slider.'",
                animationLoop: true,
                slideshowSpeed : "'.$velocita_slider.'",
                 pauseOnHover: true,
                multipleKeyboard: true,
                keyboard: true,
              
            });
        </script>';


    return $output;
}


?>