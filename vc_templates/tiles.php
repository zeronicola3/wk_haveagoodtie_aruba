<?php
/* TILES */

add_action( 'vc_before_init', 'wk_tile_build' );
function wk_tile_build() {
    vc_map( array(
        "name" => __( "Tile", "webkolm" ),
        "base" => "wk_tile",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => 'Elemento in highlight con testo foto e link',
        "class" => "wk-tile",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => "Titolo",
                'param_name' => 'wk_tile_title',
                'value' => '',
            ),
            array(
                'type' => 'textfield',
                'heading' => "Sottotitolo / Descrizione breve",
                'param_name' => 'wk_tile_subtitle',
                'description' => 'Massimo 140 caratteri'
            ),
            array(
                'type' => 'textfield',
                'heading' => "Link",
                'param_name' => 'wk_tile_link',
                'value' => 'http://',
                'description' => "Inserire l'eventuale link completo di http://"
            ),
            array(
                "type" => "attach_image",
                "holder" => "img",
                "class" => "",
                "heading" => 'seleziona immagini',
                "param_name" => "wk_tile_image",
                "value" => "",
            ),
            
        )
    ) );
}



add_shortcode( 'wk_tile', 'wk_tile_func' );
function wk_tile_func( $atts ) {
    extract( shortcode_atts( array(
        'wk_tile_title' =>  '',
        'wk_tile_subtitle' => '',
        'wk_tile_link' => '',
        'wk_tile_image' => ''
    ), $atts ) );

    $images = wp_get_attachment_image_src($wk_tile_image, 'medium')[0];

    $output='<a href="'.$wk_tile_link.'" class="tile">
    			<div class="tile_image" style="background-image:url('.$images.');"></div>
    			<div class="tile_text_wrap">
    				<h3>'.$wk_tile_title.'</h3>
    				<span>'.$wk_tile_subtitle.'</span>
    			</div>
			</a>';
   
    
    return $output;
        
}