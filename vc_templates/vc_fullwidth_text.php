<?php


add_action( 'vc_before_init', 'wk_fullwidth_text_build' );
function wk_fullwidth_text_build() {

    vc_map( array(
        "name" => __( "Webkolm FullWidth + Text", "webkolm" ),
        "base" => "webkolm_fullwidth_text",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Create a fullwidth area with text", 'webkolm'),
        "class" => "wk-fullwidth-text",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Title for textarea", "webkolm" ),
                'param_name' => 'wk_fullwidth_title',
            ),
            array(
                'type' => 'textarea',
                'value' => '',
                'heading' => __( "Text for textarea", "webkolm" ),
                'param_name' => 'wk_fullwidth_textarea',
            ),
            array(
                'type' => 'textfield',
                'heading' => "Link",
                'param_name' => 'wk_link',
                'value' => "",
                'description' => __( "Section link", "webkolm" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Select text position", "webkolm" ),
                "param_name" => "wk_fullwidth_text_position",
                "value" => array( "top-left", "top-center", "top-right", "center-left", "center-center", "center-right", "bottom-left", "bottom-center", "bottom-right" ),
                "description" => __( "Choose the position of the text", "webkolm" )
            ),
            array(
                "type" => "colorpicker",
                "heading" => __( "Select text color", "webkolm" ),
                "param_name" => "wk_text_color",
                "value" => "#000",
                "description" => __( "Color for text, defualt is black", "webkolm" )
            ),
             array(
                "type" => "attach_image",
                "holder" => "img",
                "class" => "",
                "heading" => __( "Select image", "webkolm" ),
                "param_name" => "wk_background_image",
                "value" => "",
            ),
             array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Select mobile image", "webkolm" ),
                        "param_name" => "wk_background_image_mobile",
                        "value" => "",
                        "description" => __( "Insert a background image for mobile version" , "webkolm" ),
                    ),
            array(
                "type" => "colorpicker",
                "heading" => __( "Select background color", "webkolm" ),
                "param_name" => "wk_background_color",
                "value" => "",
                "description" => __( "Color for background, defualt is white", "webkolm" )
            ),

            )   
        )
    );
}


add_shortcode( 'webkolm_fullwidth_text', 'wk_fullwidth_test_func' );
function wk_fullwidth_test_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_text_color' => '#fff',
        'wk_background_color' => '#fff',
        'wk_background_image' => '',
        'wk_background_image_mobile' => '',
        'wk_fullwidth_text_position' => 'top-left',
        'wk_fullwidth_textarea' => '',
        'wk_fullwidth_title' => '',
        'wk_link' => '',
    ), $atts ) );

    $elem_number = rand(10,9999); 

    $big_img = wp_get_attachment_image_src($wk_background_image, "full");
    $mobile_img = wp_get_attachment_image_src($wk_background_image_mobile, 'large');
    
    if($mobile_img == "") {
        $mobile_img = wp_get_attachment_image_src($wk_background_image, "large");
    }

    
    $output .= '<style>
                    .wk-fullwidth-container.wk-fullwidth-' . $elem_number . ' { 
                        background-image:url("' . $mobile_img['0'] . '");
                        background-color: ' . $wk_background_color . ';

                    }

                    .wk-fullwidth-text.wk-fullwidth-' . $elem_number . ' {
                        color: ' . $wk_text_color . ';
                    }

                    .wk-link-' . $elem_number . ' .wk-fullwidth-text b::before { background-color: ' . $wk_text_color . '; }

                    @media (min-width: 768px) {  
                        .wk-fullwidth-container.wk-fullwidth-' . $elem_number . ' { background-image:url("' . $mobile_img['0'] . '"); 
                        }
                    }
                    @media (min-width: 1400px) {  
                        .wk-fullwidth-container.wk-fullwidth-' . $elem_number . ' { background-image:url("' . $big_img['0'] . '"); 
                        }
                    }
                </style>';

    $containers = '<div class="wk-fullwidth-container wk-fullwidth-' . $elem_number . ' ">
                  <div class="wk-fullwidth-text wk-fullwidth-' . $elem_number . ' ' . $wk_fullwidth_text_position . '"><b>' . $wk_fullwidth_title . '</b><br/>' . $wk_fullwidth_textarea . '</div>
                </div>';

    if($wk_link != "") {
        $output .= '<a href="' . $wk_link . '" class="wk-link-fullwidth wk-link-' . $elem_number . '" >' . $containers . '</a>';
    } else {
        $output .= $containers;
    }

    return $output;
}

?>