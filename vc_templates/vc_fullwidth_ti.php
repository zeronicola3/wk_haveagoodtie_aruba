<?php


add_action( 'vc_before_init', 'wk_fullwidth_ti_build' );
function wk_fullwidth_ti_build() {

    vc_map( array(
        "name" => __( "Webkolm FullWidth Text + Images", "webkolm" ),
        "base" => "webkolm_fullwidth_ti",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Create a fullwidth area with text and two images columns", 'webkolm'),
        "class" => "wk-fullwidth-ti",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Title for textarea", "webkolm" ),
                'param_name' => 'wk_fullwidth_title',
            ),
            array(
                'type' => 'textarea',
                'value' => '',
                'heading' => __( "Text for textarea", "webkolm" ),
                'param_name' => 'wk_fullwidth_textarea',
            ),
            array(
                "type" => "colorpicker",
                "heading" => __( "Select text color", "webkolm" ),
                "param_name" => "wk_text_color",
                "value" => "#000",
                "description" => __( "Color for text, defualt is black", "webkolm" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Select large column", "webkolm" ),
                "param_name" => "wk_fullwidth_large_column",
                "value" => array( "left","right" ),
                "description" => __( "Choose the large column", "webkolm" )
            ),
            //PARAMS GROUP
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'images',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Select image", "webkolm" ),
                        "param_name" => "wk_image",
                        "value" => "",
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => "Immagine a piena larghezza",
                        "param_name" => "wk_image_not_fullwidth",
                        "value" => array(
                             'Disabilita'=>'yes',
                        ), //value
                    ),
                )
                )   
            )
        )
    );
}


add_shortcode( 'webkolm_fullwidth_ti', 'wk_fullwidth_ti_func' );
function wk_fullwidth_ti_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_fullwidth_title' => '',
        'wk_fullwidth_textarea' => '',
        'wk_text_color' => '',
        'wk_fullwidth_large_column' => 'left',
    ), $atts ) );

    $images= vc_param_group_parse_atts( $atts['images'] );

    // RANDOM ID ELEMENT
    $id_elem=rand(0,99999);

    $output .= '
        <div class="wk-text-container wk-text-container-' . $id_elem . '">
            <div class="wk-text"><b>' . $wk_fullwidth_title . '</b><br/>' . $wk_fullwidth_textarea . '</div>
        </div>';

    $output .= '
        <div class="wk-images-container wk-image-container-' . $id_elem . ' wk-' . $wk_fullwidth_large_column . '-column">';
    $numslide=0;

    // CICLO LE IMMAGINI
    foreach( $images as $image ){
        $med_img = wp_get_attachment_image_src($image['wk_image'], "large");
        $big_img = wp_get_attachment_image_src($image['wk_image'], "big");


        // IMAGE CONTAINER CLASSES
        $img_classes .= 'wk-image-' . $numslide  . ' ';
        if($image['wk_image_not_fullwidth'] != "") {
            $img_classes .= 'wk_image_not_fullwidth ';
        }
        if($numslide == 0) {
            $img_classes .= 'wk-image-first ';
        }
        if(($numslide % 2) == 0) {
            $img_classes .= 'wk-image-dx ';
        } else {
            $img_classes .= 'wk-image-sx ';
        }


        $output .= '
            <div class="wk-img-container ' . $img_classes . '">
                <img src="' . $med_img['0'] . '" class="wk-image-item nowide"/>
                <img src="' . $big_img['0'] . '" class="wk-image-item onlywide"/>
            </div>
        ';

        $numslide++;
        $img_classes = "";
        
    }

    $output .= '
        </div>';

    return $output;
}


?>