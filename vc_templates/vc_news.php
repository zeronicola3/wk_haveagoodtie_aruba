<?php


add_action( 'vc_before_init', 'wk_news_build' );
function wk_news_build() {

    vc_map( array(
        "name" => __( "Webkolm news", "webkolm" ),
        "base" => "webkolm_news",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Create a three posts news area", 'webkolm'),
        "class" => "wk-news",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Title for news area", "webkolm" ),
                'param_name' => 'wk_news_title',
            ),
            array(
                'type' => 'textfield',
                'value' => '99999999',
                'heading' => __( "View posts not older than the days declared", "webkolm" ),
                'param_name' => 'wk_days_delay',
            ),
            )   
        )
    );
}



add_shortcode( 'webkolm_news', 'wk_news_test_func' );
function wk_news_test_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_news_title' => '',
        'wk_days_delay' => '99999999',
    ), $atts ) );

    global $post;

        $postslist = get_posts( array(
            'posts_per_page' => 3,
            'order'          => 'DSC',
            'orderby' => 'date', 
            'suppress_filters' => 0,
        ) );

        $post_nmb = 0;

        // Get the current date
         $current_date = current_time( 'Y-m-d', $gmt = 0 );
         $current_date = new DateTime($current_date);

        if ( count($postslist) > 2  ) {

            $post_date = get_the_date( 'Y-m-d', $postslist[0]->ID );
            $post_date = new DateTime($post_date);
            $interval = $current_date->diff($post_date);
               if( $interval->days < $wk_days_delay ) { 

        $output .= '
             <div class="news-wrapper wrapper">
                <h3><a href="' . esc_url( get_page_link( 7 ) ) . '">' . $wk_news_title . '</a></h3>';

                    foreach ( $postslist as $post ) :
                        setup_postdata( $post );

                        $output .= '
                        <a href="' . get_the_permalink() . '" class="news-item">';
                        
                            if( has_post_thumbnail()) {
                                  
                                  $id_immagine = get_post_thumbnail_id($post->ID);
                                  $thumb = wp_get_attachment_image_src( $id_immagine, 'medium' );
                                  $thumb_big = wp_get_attachment_image_src( $id_immagine, 'full' );

                                $output .= '
                                  <div class="post-image-item img-c-' . $post_nmb . '">
                                    <style>
                                      .post-image-item.img-c-' . $post_nmb . '{ background-image:url("' . $thumb['0'] .'");}
                                    </style>
                                  </div>';
                            }

                            $output .= '

                            <h5 class="news-title">' . get_the_title() . '</h5>
                        </a>';

                    $post_nmb++;
                    endforeach; 
                    $output .= '</div>';
            }
        }
            wp_reset_postdata();

    return $output;
}

?>