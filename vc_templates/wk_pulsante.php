<?php 

/* PULSANTE VK */

add_action( 'vc_before_init', 'wk_button_build' );
function wk_button_build() {
    vc_map( array(
        "name" => __( "Button", "webkolm" ),
        "base" => "wk_button",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Insert button", 'webkolm'),
        "class" => "wk_button",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => "Text",
                'param_name' => 'wk_text',
                'value' => "",
                'description' => __( "Button text", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'heading' => "Link",
                'param_name' => 'wk_link',
                'value' => "",
                'description' => __( "Button link", "webkolm" )
            ),
            array(
                "type" => "colorpicker",
                "heading" => __( "Color of the button", "webkolm" ),
                "param_name" => "primary_color",
                "value" => "",
                "description" => __( "Default is white", "webkolm" )
            ),
            array(
                "type" => "colorpicker",
                "heading" => __( "Color for hover button", "webkolm" ),
                "param_name" => "secondary_color",
                "value" => "",
                "description" => __( "Default is black", "webkolm" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Select alignment", "webkolm" ),
                "param_name" => "wk_alignment",
                "value" => array( "left", "center", "right" ),
                "description" => __( "Choose the alignment of the buttons (default left)", "webkolm" )
            )
            
        )
    ) );
}



add_shortcode( 'wk_button', 'wk_button_func' );
function wk_button_func( $atts ) {
    extract( shortcode_atts( array(
        'wk_text' => '',
        'wk_link' => '',
        'wk_alignment' => '',
        'primary_color' => '',
        'secondary_color' => ''
    ), $atts ) );

    // RANDOM ID SLIDER
    $id_button=rand(0,99999);

    // COLOR
    if($primary_color==""){
        // STANDARD COLOR
        $primary_color="#fff";
    }

    if($secondary_color==""){
        // HOVER COLOR
        $secondary_color="#000";
    }
    


    $output .= '<div class="wrap_pulsante wk_align_'.$wk_alignment.'"><a href="'.$wk_link.'" class="pulsante wk-pulsante-' . $id_button . '" >
                '.$wk_text.'
            </a></div>
            <style>
                .wk-pulsante-' . $id_button . ' { color:'.$primary_color.';  }
                .wk-pulsante-' . $id_button . ':hover { background: '.$primary_color.'; color: '.$secondary_color.'; }
                .wk-pulsante-' . $id_button . '::before { background: '.$primary_color.'; }
                .wk-pulsante-' . $id_button . '::after { background: '.$primary_color.'; }
            </style>

            ';

    return $output;
        
}

?>