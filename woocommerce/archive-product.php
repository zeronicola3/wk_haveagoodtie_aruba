<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<div id="contenuti" class="shop">
	<div class="wrapper">
		<?php
			/**
			 * woocommerce_before_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 * @hooked WC_Structured_Data::generate_website_data() - 30
			 */
			do_action( 'woocommerce_before_main_content' );
		?>

			<?php if ( have_posts() ) : ?>

				<?php
					/**
					 * woocommerce_before_shop_loop hook.
					 *
					 * @hooked wc_print_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					//do_action( 'woocommerce_before_shop_loop' );
				?>

					<?php //woocommerce_product_subcategories(); 

						add_action( 'azione_aggiungi_notice', 'wc_print_notices' );
					//	add_action( 'azione_aggiungi_count', 'woocommerce_result_count' );
					//	do_action( 'azione_aggiungi_count' );
					?>

					<?php 
					// CONTATORE POST (VA SEMPRE FUORI DAL CICLO)
					$post_nmb = 0;
					
					while ( have_posts() ) : the_post(); ?>

						<?php
							/**
							 * woocommerce_shop_loop hook.
							 *
							 * @hooked WC_Structured_Data::generate_product_data() - 10
							 */
							do_action( 'woocommerce_shop_loop' );

							// In the product loop:

							//$variations = $product->get_available_variations();

							// Outside the product loop:
							
							$product = new WC_Product_Variable( $post->ID );
							$variations = $product->get_available_variations();


							$shop_desc_title = types_render_field("titolo-aggiuntivo-prodotto", array('post_id' => $post->ID, "raw"=>"true"));
							$shop_desc = types_render_field("descrizione-aggiuntiva-prodotto", array('post_id' => $post->ID, "raw"=>"false"));
							?>

							<div class="shop-product-desc-container">
								<div class="shop-product-desc">
									<b><?php echo $shop_desc_title; ?></b>
									<?php echo $shop_desc; ?>
								</div>
							</div>

							<ul class="shop-items-list wkrow">

							<?php 

							// METTO CONTATORE DELLE VARIAZIONI PER DIFFERENZIARE LE CLASSI DA ASSOCIARE VIA CSS
							$varnumb = 0;
							$array_url=array();

							foreach($variations as $var) : setup_postdata($var);


								//$variation = new WC_Product_Variation($var['variation_id']);
								//echo "<pre>"; print_r($var); echo "</pre>"; 

								$taxonomy = 'pa_color';
								$meta = get_post_meta($var['variation_id'], 'attribute_'.$taxonomy, true);
								$term = get_term_by('slug', $meta, $taxonomy);
								$variation_name = $term->name;
								$var_link = get_permalink() . '?attribute_pa_color=' . $term->slug;
								$array_url[$varnumb]=$var_link;
								$onsale = 0;

								

								if($var['display_price'] != $var['display_regular_price']) {
									$onsale = 1;
								}
							?>

							    <li class="shop-item-image wkcol-4 ">
							      <style>
							        .shop-item-image .shop-image-<?php echo $post_nmb ?>-<?php echo $varnumb ?>{ background-image:url('<?php echo $var['image']['src'] ?>');}
							         @media (min-width: 768px) {  .shop-item-image .shop-image-<?php echo $post_nmb ?>-<?php echo $varnumb ?> { background-image:url('<?php echo $var['image']['src'] ?>'); } }
							      </style>
							      <a href="<?php echo $var_link; ?>" class="shop-image shop-image-<?php echo $post_nmb ?>-<?php echo $varnumb ?>">
							      	<?php if($onsale != 0) { ?> 
							      		<div class="onsale-sticker"><?php _e("ON SALE","webkolm"); ?></div>
							      	<?php } ?>
							      	<div class="shop-item-text">
						      			<span class="shop-item-title"><?php echo $variation_name; ?></span>
						      			
				      					<span class="shop-item-reg-price <?php if($onsale != 0) { echo "onsale"; }?>"><?php echo $var['display_regular_price'] . ' ' .get_woocommerce_currency_symbol( $currency ); ?></span>
						      			
						      			<?php if($onsale != 0) { ?>
							      			<span class="shop-item-price">
							      				<?php echo $var['display_price'] . ' ' .get_woocommerce_currency_symbol( $currency ); ?>
							      			</span>
						      			<?php } ?>
							      	</div>
							      </a>

							    </li>


							<?php  

							// AUMENTO NUMERO DELLA VARIAZIONE
							$varnumb++;

							endforeach; ?>

							</ul>
								<br/>
							    <br/>
							    	<br/>
							        <br/>
						<?php //wc_get_template_part( 'content', 'product' ); ?>

					<?php 

					// IL CONTATORE DEL POST NUMBER VA FUORI DAL CICLO DELLE VARIAZIONI
					$post_nmb++;

					endwhile; // end of the loop. ?>

			

			<?php endif; ?>
	</div>
</div>
<?php 

// TEST SITEMAP
$doc = new DOMDocument( '1.0');
        $doc->formatOutput = true;
        $sitemapindex = $doc->appendChild($doc->createElement('sitemapindex'));
        $data_sitemap=date(DATE_ATOM, mktime());
        foreach ($array_url as $wk_sitemap_url) {
        	$sitemap = $sitemapindex->appendChild($doc->createElement('sitemap'));
            $loc = $sitemap->appendChild($doc->createElement('loc'));
            $loc->nodeValue = $wk_sitemap_url;
            $lastmod = $sitemap->appendChild($doc->createElement('lastmod'));
            $lastmod->nodeValue = $data_sitemap;
        }
          
          
        if(ICL_LANGUAGE_CODE=='it'){
        	$doc->save('sitemap_wc_products.xml');
        }else{
        	$doc->save('sitemap_wc_products_en.xml');
        }
        
?>
<?php get_footer( 'shop' ); ?>
