<?php
/**
 * Cart item data (when outputting non-flat)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-item-data.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version 	2.4.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $wk_tempi_spedizione;
global $wk_tempi_spedizione_custom;
if($wk_tempi_spedizione<$wk_tempi_spedizione_custom['0']){
	$wk_tempi_spedizione=$wk_tempi_spedizione_custom['0'];
}
?>
<dl class="variation">
	<?php // foreach ( $item_data as $data ) : 
		$data = $item_data['1']; 
			if($data['display'] != "") {
				if(strpos($data['display'], "\n") !== FALSE) {

					$data['display'] = str_replace("\n", "<br/>", $data['display']);
				}

		?>	
				<dd class="variation-text"><?php echo '"' . $data['display'] . '"'; ?></dd>
	<?php 	}// endforeach; ?>
</dl>
