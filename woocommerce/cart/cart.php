<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();
global $wk_tempi_spedizione;
global $wk_tempi_spedizione_custom;

$wk_tempi_spedizione=0;
$wk_tempi_spedizione_custom=0;

//do_action( 'woocommerce_before_cart' ); ?>

<div class="wrapper wkrow">
	<div class="wkcol-1"></div>
	<div class="wkcol-10">
		<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
			<?php do_action( 'woocommerce_before_cart_table' ); ?>

			<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
				<thead>
					<tr>
						<th colspan="2" class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
						<th class="product-quantity"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
						<th class="product-price"><?php _e( 'Price', 'woocommerce' ); ?></th>
						<th class="product-subtotal"><?php _e( 'Total', 'woocommerce' ); ?></th>
						<th class="product-remove">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php do_action( 'woocommerce_before_cart_contents' ); ?>

					<?php
					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
							
							$id = icl_object_id($_product->get_id(), 'product', false,'en');
							$tempi_spedizione= get_post_meta($id,"wpcf-tempi-di-spedizione");
							//echo $_product->get_id();

							$wk_tempi_spedizione_custom=get_post_meta($id,"wpcf-tempi-di-spedizione-con-personalizzazione");
							//echo $wk_tempi_spedizione_custom['0'];

							if($wk_tempi_spedizione<$tempi_spedizione['0']){
								$wk_tempi_spedizione=$tempi_spedizione['0'];
							}

							?>
							<tr class="woocommerce-cart-form__cart-item table-items <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

								<td class="product-thumbnail">
									<?php
										$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

										if ( ! $product_permalink ) {
											echo $thumbnail;
										} else {
											printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
										}
									?>
								</td>

								<td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
									<?php

										//  CUSTOM ITEM NAME 
										$taxonomy = 'pa_color';
										$meta = get_post_meta($_product->get_ID(), 'attribute_' . $taxonomy, true);
										$term = get_term_by('slug', $meta, $taxonomy);

										$product_title = $term->name . "<br/>" . $_product->get_title();

										if ( ! $product_permalink ) {
											echo apply_filters( 'woocommerce_cart_item_name', $product_title, $cart_item, $cart_item_key ) . '&nbsp;';
										} else {
											echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $product_title ), $cart_item, $cart_item_key );
										}

										// Meta data
										echo WC()->cart->get_item_data( $cart_item );

										// Backorder notification
										if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
											echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
										}
									?>
								</td>

								<td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
									<?php
										if ( $_product->is_sold_individually() ) {
											$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
										} else {
											$product_quantity = woocommerce_quantity_input( array(
												'input_name'  => "cart[{$cart_item_key}][qty]",
												'input_value' => $cart_item['quantity'],
												'max_value'   => $_product->get_max_purchase_quantity(),
												'min_value'   => '0',
											), $_product, false );
										}

										echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
									?>
								</td>

								<td class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
									<?php
										echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
									?>
								</td>

								<td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
									<?php
										echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
									?>
								</td>


								<td class="product-remove">
									<?php

									$close_svg = '<svg id="close-x" x="0px" y="0px" viewBox="0 0 194 194" enable-background="new 0 0 194 194" xml:space="preserve"><g><g><rect x="91.2" y="-34.4" transform="matrix(0.7071 0.7071 -0.7071 0.7071 97.0045 -40.1779)" fill="#000" width="11.6" height="262.8"/></g><g><rect x="-34.4" y="91.2" transform="matrix(0.7071 0.7071 -0.7071 0.7071 97.0073 -40.1779)" fill="#000" width="262.8" height="11.6"/></g></g></svg>';

										echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
											'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">' . $close_svg . '</a>',
											esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
											__( 'Remove this item', 'woocommerce' ),
											esc_attr( $product_id ),
											esc_attr( $_product->get_sku() )
										), $cart_item_key );
									?>
								</td>
							</tr>
							<?php
						}
					}

					?>
					<tr class="final-row woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
						
						<td style="border-top: 1px solid black;" colspan="2" class="actions">

							<?php if ( wc_coupons_enabled() ) { ?>
								<div class="cell-title">Inserisci il tuo codice promozionale</div>
								<div class="coupon">
									<label for="coupon_code"><?php _e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'OK', 'woocommerce' ); ?>" />
									<?php do_action( 'woocommerce_cart_coupon' ); ?>
								</div>
							<?php } ?>

						</td>

						<td style="border-top: 1px solid black;" colspan="2" class="product-price" >
							<input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>" />

							<?php do_action( 'woocommerce_cart_actions' ); ?>

							<?php wp_nonce_field( 'woocommerce-cart' ); ?>
						</td>

						<td  style="border-top: 1px solid black;" class="total-order-sub" data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>">
							<div class="nomobile cell-title subtotal-title"><?php _e("Subtotal","woocommerce"); ?></div>
							<div class="cell-content"><?php wc_cart_totals_subtotal_html(); ?></div>
						</td>

						<td style="border-top: 1px solid black;" class="product-remove"></td>

					</tr>

					<?php do_action( 'woocommerce_cart_contents' ); ?>


					<?php do_action( 'woocommerce_after_cart_contents' ); ?>
				</tbody>
			</table>
			<?php do_action( 'woocommerce_after_cart_table' ); ?>
		</form>

		<div class="cart-collaterals">
	<?php
		/**
		 * woocommerce_cart_collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
	 	do_action( 'woocommerce_cart_collaterals' );
	?>

		</div>



<?php do_action( 'woocommerce_after_cart' ); ?>