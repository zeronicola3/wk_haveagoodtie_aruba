

<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }


	 if(isset($_GET['attribute_pa_color'])) {
	 	$color_attr = $_GET['attribute_pa_color'];
	 } else { 
		$color_attr = "undefined";
	 } ?>


<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="product-thumb-container">
		<?php
			/**
			 * woocommerce_before_single_product_summary hook.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			// do_action( 'woocommerce_before_single_product_summary' );

			//echo $post->variation_id;
	
		$args = array(
			'post_type'     => 'product_variation',
			'post_status'   => array( 'private', 'publish' ),
			'numberposts'   => -1,
			'orderby'       => 'menu_order',
			'order'         => 'asc',
			'post_parent'   => $post->ID // get parent post-ID
		);
		$variations = get_posts( $args );

		$var_nmb = 0;
		$descrizioni_prodotti = "";

		foreach ( $variations as $variation ) {

			$post_id = $post->ID;
			// get variation ID
			$variation_id = $variation->ID;

			// get variations meta
			$product_variation = new WC_Product_Variation( $variation_ID );

			$image_ids = get_post_meta( $variation_id, '_wc_additional_variation_images', true );

			

			$image_ids = explode( ',', $image_ids );

			$product = wc_get_product( $variation_id );

			$main_image_id = "";

			// If we're selecting the "Choose an Option" (i.e. no variation), product may not be set
			if ( $product ) {
				$main_image_id = $product->get_image_id();

				if ( !empty( $image_ids['0'] ) ) {
					array_unshift( $image_ids, $main_image_id );
				} else {
					$image_ids['0'] = $main_image_id;
				}
			}

			// GET VARIATIONS NAMES AND SLUGS

			$taxonomy = 'pa_color';
			$meta = get_post_meta($variation_id, 'attribute_' . $taxonomy, true);
			$term = get_term_by('slug', $meta, $taxonomy);

			$desc_var = get_post_meta( $variation_id, '_variation_description', true );

			$gallery_switcher = "";
			$desc_switcher = "";

			if($term->slug == $color_attr) { 
				$gallery_switcher = "da_attivare";
				$desc_switcher = "activate";
				$var_name = $term->name;
				$var_slug = $post->post_name;
			} else {
				if((!isset($_GET['attribute_pa_color'])) && ((count($variations )- 1) == $var_nmb)) {
					$gallery_switcher = "da_attivare";
					$desc_switcher = "activate";
					$var_name = $term->name;
					$var_slug = $post->post_name;
				}
			}

			if($desc_var != "") { 
				$descrizioni_prodotti .= 
				'<div class="descrizione-singolo-prodotto descrizione-' . $term->slug . ' ' . $desc_switcher . '" style="display: none;">' .  apply_filters('the_content', $desc_var) . '<br/></div>';
			} else {
				$descrizioni_prodotti .= 
				'<div class="descrizione-singolo-prodotto descrizione-' . $term->slug . ' ' . $desc_switcher . '" style="display: none;">' . get_the_excerpt() . '<br/></div>';
			} 
			
			$main_images = '<div class="webkolm-product-gallery gallery-' . $term->slug . ' ' . $gallery_switcher . '" style="display:none"><ul class="slides">';

			$loop = 0;

					if ( 0 < count( $image_ids ) ) {
						// build html
						foreach ( $image_ids as $id ) {

							$image_title = esc_attr( get_the_title( $id ) );
							$large_size_image  = wp_get_attachment_image_src( $id, 'large' );
							$medium_size_image  = wp_get_attachment_image_src( $id, 'medium' );

							$attributes = array(
								'title'                   => $image_title,
								'data-large_image'        => $full_size_image[0],
								'data-large_image_width'  => $full_size_image[1],
								'data-large_image_height' => $full_size_image[2],
							);

							// see if we need to get the first image of the variation
							// only run one time
							
							// build the list of variations as main images in case a custom theme has flexslider type lightbox
							$main_images .= '<li class="product-image-item ' . $term->slug . '-img-p-' . $loop . '" data-thumb="' . $medium_size_image['0'] . '">
												<img class="product-single-image nomobile" src="' . $large_size_image['0'] . '" />
				                            	<style>
					                              .product-image-item.' . $term->slug . '-img-p-' . $loop . ' { 
					                              		background-image:url("' . $medium_size_image['0'] . '");
					                              		background-repeat: no-repeat;
					                              }
				                            	</style>
				                          	  </li>
				                          	  ';

							$loop++;
						} // End foreach().
					} else {// End if().

						$id = $image_ids['0'];

						$image_title = esc_attr( get_the_title( $id ) );
						$large_size_image  = wp_get_attachment_image_src( $id, 'large' );
						$medium_size_image  = wp_get_attachment_image_src( $id, 'medium' );

						$html  = '<li class="product-image-item ' . $term->slug . '-img-p-' . $loop . '">
			                    	<style>
			                          .product-image-item.' . $term->slug . '-img-p-' . $loop . ' {     background-repeat: no-repeat; background-image:url("' . $medium_size_image['0'] . '");}
			                            @media (min-width: 768px) {  .product-image-item.' . $term->slug . '-img-p-' . $loop . ' { background-image:url("' . $large_size_image['0'] . '"); } }
			                    	</style>
			                  	  </li>
			                  	  ';
					}

					$main_images .= '</ul></div>';

					echo $main_images;



			//print_r($image_ids);

			// to get variation meta, simply use get_post_meta() WP functions and you're done 
			// ... do your thing here
			$var_nmb ++;
		}
		
		?>

		<style>
			@media screen and (max-width: 768px) {
				.webkolm-product-gallery ul.slides li:last-child::after {
					position: absolute;
					width: 100%;
					height: 100%;
					opacity: .8;
					background-color: white;
					content: " ";
					z-index: -1;

				}
			    .webkolm-product-gallery ul.slides li:last-child::before {
			    	content: "<?php _e('Personalize \a your tie', "webkolm");?>";
				    position: absolute;
				    width: 40%;
				    left: 50%;
				    color: black;
				    top: 50%;
				    transform: translateX(-50%) translateY(-50%);
				    text-align: center;
				    text-transform: uppercase;
				    font-size: 1rem;
				    line-height: 1.2em;
			    }
			}
		</style>

	</div>

	<div class="summary entry-summary">

		<?php
			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10 -

			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30 spostare
			 * @hooked woocommerce_template_single_meta - 40 -
			 * @hooked woocommerce_template_single_sharing - 50 -
			 * @hooked WC_Structured_Data::generate_product_data() - 60 ?
			 */

			if($var_name != ""){
				the_title( '<h1 class="product_title entry-title" data-product="' . $var_slug . '"><span class="color-attr">' . $var_name . '</span><br/><span class="product-title">', '</span></h1>' );
			} else {
				add_action( 'azione_aggiungi_title', 'woocommerce_template_single_title' );
				do_action( 'azione_aggiungi_title' );
			}

			

			?>

			<div class="single-product-desc"><?php echo $descrizioni_prodotti; ?></div>

		<?php
			add_action( 'azione_aggiungi_variazioni_add_cart', 'woocommerce_template_single_add_to_cart' );
			do_action( 'azione_aggiungi_variazioni_add_cart' );


			//do_action( 'woocommerce_single_product_summary' );
		?>

	</div><!-- .summary -->
</div><!-- #product-<?php the_ID(); ?> -->
	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10 -
		 * @hooked woocommerce_upsell_display - 15 -
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
		the_content();
	?>



<?php do_action( 'woocommerce_after_single_product' ); ?>
