<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;

get_header( 'shop' ); ?>
<div id="contenuti" class="single-product">
	<div class="wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php  wc_get_template_part( 'content', 'single-product' ); ?>

			<?php

			    $args = array( 
			    	'post_type' => 'product',
			        'post_status' => 'publish',
			        'fields' => 'ids', 
			        'exclude' => $post->ID,
			        'suppress_filters' => 0,
			    );
			    $post_is = get_posts( $args );


			    echo $_GET['attribute_pa_color'];

				// In the product loop:
				//$variations = $product->get_available_variations();

				// Outside the product loop:

				$product = new WC_Product_Variable( $post_is[0] );
				$variations = $product->get_available_variations(); 
				print_r($product->ID);
				$post_nmb = 0;

				

				//if($variation!="") {

					?>
					<h5><?php echo __("Also look","webkolm");?></h5>
					<ul class="shop-items-list wkrow">

					<?php foreach($variations as $var) : setup_postdata($var);


						//$variation = new WC_Product_Variation($var['variation_id']);
						//echo "<pre>"; print_r($var); echo "</pre>"; 



						$taxonomy = 'pa_color';
						$meta = get_post_meta($var['variation_id'], 'attribute_'.$taxonomy, true);
						$term = get_term_by('slug', $meta, $taxonomy);
						$variation_name = $term->name;
						// MAI PRENDERE IL PERMALINK IN UN MULTILINGUA MA PASSAR SEMPRE PER LE FUNZIONI DI WPML

						//if( ICL_LANGUAGE_CODE == 'en') {
					//		$var_link=get_permalink( icl_object_id($post_is[0], 'product', false) ). '?attribute_pa_color=' . $term->slug . '-en';
					//	} else {
							$var_link=get_permalink( icl_object_id($post_is[0], 'product', false) ). '?attribute_pa_color=' . $term->slug;
					//	}
						
						$onsale = 0;
						

						if($var['display_price'] != $var['display_regular_price']) {
							$onsale = 1;
						}
					?>

					    <li class="shop-item-image wkcol-3">
					      <style>
					        .shop-item-image .shop-image-<?php echo $post_nmb ?>{ background-image:url('<?php echo $var['image']['src'] ?>');}
					         @media (min-width: 768px) {  .shop-item-image .shop-image-<?php echo $post_nmb ?> { background-image:url('<?php echo $var['image']['src'] ?>'); } }
					      </style>
					      <a href="<?php echo $var_link; ?>" class="shop-image shop-image-<?php echo $post_nmb ?>">
      				      	<?php if($onsale != 0) { ?> 
      				      		<div class="onsale-sticker"><?php _e("ON SALE","webkolm"); ?></div>
      				      	<?php } ?>
      				      	<div class="shop-item-text">
      			      			<span class="shop-item-title"><?php echo $variation_name; ?></span>
      			      			
      	      					<span class="shop-item-reg-price <?php if($onsale != 0) { echo "onsale"; }?>"><?php echo $var['display_regular_price'] . ' ' .get_woocommerce_currency_symbol( $currency ); ?></span>
      			      			
      			      			<?php if($onsale != 0) { ?>
      				      			<span class="shop-item-price">
      				      				<?php echo $var['display_price'] .' ' .get_woocommerce_currency_symbol( $currency ); ?>
      				      			</span>
      			      			<?php } ?>
      				      	</div>
					      </a>



					    </li>

					<?php $post_nmb++; 
					if($post_nmb > 3) { break; }
					endforeach; ?>

					</ul>

			<?php//  } //wc_get_template_part( 'content', 'product' ); ?>

		<?php endwhile; // end of the loop. ?>


	</div>
</div>


<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
